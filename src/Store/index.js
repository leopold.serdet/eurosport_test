import { createStore, applyMiddleware } from "redux";
import combineReducers from "./reducers/index";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";

//create logger Middleware
const loggerMiddleware = createLogger();
//middlewares
const middlewares = [thunk, loggerMiddleware];
//create store 
const store = createStore(combineReducers, applyMiddleware(...middlewares));

export default store; 