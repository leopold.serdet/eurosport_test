import React from 'react';
import { Card, Avatar } from 'antd';
const { Meta } = Card;

export default function card(props) {
	const { firstname, lastname, country, picture, data } = props.player;
	return (
		<Card
			style={{ marginLeft: '10%', maxWidth: 300 }}
			cover={<img alt='player' src={picture} style={{maxHeight: 300}} />}>
			<Meta
				avatar={<Avatar src={country.picture} />}
				title={<h3>{firstname + ' ' + lastname}</h3>}
			/>
			<br />
			<Meta title='Rank' description={data.rank} />
            <hr/>
			<Meta title='Points' description={data.points} />
			<hr />
			<Meta title='Weight' description={data.weight / 1000 + ' Kg'} />
			<hr />
			<Meta title='Wins' description={data.last.filter((w=>w===1)).length} />
			<hr />
			<Meta title='Losses' description={data.last.filter((w=>w===0)).length} />
		</Card>
	);
}
