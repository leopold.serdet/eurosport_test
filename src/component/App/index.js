import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchTennisPlayers } from './../../Store/thunks/fetchTennisPlayers';
import Card from './../shared/Card';
import logo from './logo.svg';
import { Spin, Alert } from 'antd';
import './style.css';

function App() {
	const { isFetching, error, players } = useSelector(
		state => state.tennisPlayers
	);
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(fetchTennisPlayers());
	}, [dispatch]);
	return (
		<div className='App'>
			<header className='App-header'>
				<img src={logo} className='App-logo' alt='logo' />
			</header>
			<Spin spinning={!!isFetching} tip='Tennis Players Info...'>
				<div className='grid'>
					{!isFetching &&
						players.map((player, i) => <Card player={player} key={i} />)}
				</div>
				{!!error && (
					<Alert
						message={error.code}
						description={error.message}
						type='error'
						closable
					/>
				)}
			</Spin>
		</div>
	);
}
export default App;
